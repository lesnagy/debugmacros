#
# Library configuration file used by dependent projects.
# @file:   debugmacros-config.cmake
# @author: L. Nagy
#

if (NOT DEFINED debugmacros_FOUND)

  # Locate the library headers.
  find_path(debugmacros_include_dir
    NAMES DebugMacros.h
    PATHS ${debugmacros_DIR}/include
  )

  # Handle REQUIRED, QUIED and version related arguments to find_package(...)
  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(
    debugmacros DEFAULT_MSG
    debugmacros_include_dir
  )
endif()
